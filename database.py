#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psycopg2
from cfg import DB, USER, PASSWORD, HOST, PORT, DELETED_USER, DEBUG
from constants import POST, COMMENT


class DatabaseError(Exception):
    pass


class Database:
    connection = None
    cursor = None
    reco_tries = 0
    def _connection_handler(fun):
        """
        Execute a function, tries to reconnect in case of an error,
        commit the changes
        """
        def n_fun(self, *args, **kwargs):
            try:
                if self.connection is None:
                    self.connect()
                return fun(self, *args, **kwargs)
            except psycopg2.OperationalError, err:
                self.connect(True, err)
                return n_fun(self, *args, **kwargs)
                # After 3 tries, an error is raised
            finally:
                if self.connection is not None:
                    self.connection.commit()
        return n_fun

    @_connection_handler
    def execute(self, req, args=[]):
        if DEBUG:
            print "%s %% %s" % (req, args)
        cur = self.cursor
        cur.execute(req, args)

    @_connection_handler
    def filter(self, table, one=False, columns="*", *conditions):
        """
        If 'one' is True, returns the first result or None. Else, return a list.
        Conditions are pure SQL code :
            "ORDER BY foo DESC", "LIMIT 0, 50", "WHERE foo='bar' OR ..."

        /!\ This function does not avoid SQL injections. Take measures before
        creating the conditions.
        """
        def s(val):
            if isinstance(val, str):
                return val.decode("utf-8", "ignore")
            else:
                return val
        if isinstance(columns, str):
            col = columns
        else:
            col = ", ".join(columns)
        cond = " ".join(conditions)
        # The connection_handler has created a cursor
        self.execute("SELECT %(col)s FROM %(table)s %(cond)s;" % locals())
        # create a dict for each row
        cur = self.cursor
        rv = [dict((cur.description[idx][0], s(value)) for idx, value in 
            enumerate(row)) for row in cur.fetchall()]
        return (rv[0] if rv else None) if one else rv
        return cur.fetchone() if one else cur.fetchall()

    @_connection_handler
    def insert(self, table, **values):
        """
        Insert a new entry in the table

        This function is safe from SQL injections :
            cursor.execute from the module psycopg2 avoid it.
        """
        if not values:
            raise DatabaseError("Values needed to create a new entry.")
        items = values.items()
        keys = ", ".join(i.strip("'\"") for i in zip(*items)[0])
        ph = ", ".join("%s" for i in items)
        query = "INSERT INTO %s (%s) VALUES (%s);" % (table, keys, ph)
        self.execute(query, zip(*items)[1])

    @_connection_handler
    def edit(self, table, where, **modif):
        """
        edit("user", {"id":3}, name="bar", age=30)
        """
        conditions = " AND ".join(self.escape(("%s=%%s" % k), [v]) 
                for k, v in where.items())
        modifications = ", ".join(self.escape(("%s=%%s" % k), [v])
                for k, v in modif.items())
        query = "UPDATE %s SET %s WHERE %s;" % (table, modifications, conditions)
        self.execute(query)

    @_connection_handler
    def delete(self, table, *cond):
        """
        delete("user", "WHERE id=3")
        """
        query = "DELETE FROM %s %s;" % (table, " ".join(cond))
        self.execute(query)

    def connect(self, reconnect=False, error=None):
        """
        Creates a connection to the database and a cursor
        The number of reconnection is 3. After that, the function raises
        an error.
        """
        if self.connection is None:
            self.connection = psycopg2.connect(database=DB, user=USER,
                    password=PASSWORD, host=HOST, port=PORT)
        if reconnect:
            self.reco_tries += 1
        if self.reco_tries > 3:
            raise psycopg2.DatabaseError(error.pgerror)
        self.cursor = self.connection.cursor()

    def disconnect(self):
        if self.connection is not None:
            try:
                self.connection.close()
            except psycopg2.InterfaceError:
                pass
            self.connection = None
        if self.cursor is not None:
            try:
                self.cursor.close()
            except psycopg2.InterfaceError:
                # Already closed
                pass
            self.cursor = None

    def fmt(self, act, sep=" AND ", **args):
        """
        very light formatter for conditions in SQL requests
        fmt("WHERE", foo=3, bar_lt=5) -> WHERE foo=3 AND bar<5
        operators are lt, gt, neq
        """
        cond = []
        for k, v in args.items():
            pref = ""
            op = ""
            if k.endswith("lt"):
                op = "<"
            elif k.endswith("gt"):
                op = ">"
            elif k.endswith("neq"):
                pref = "NOT "
                op = "="
            elif k.endswith("eq"):
                op = "="
            if op:
                k = k.rsplit("_", 1)[0]
            else:
                op = "="
            cond.append(self.escape(("%s%s%s%%s" % (pref, k, op)), [v]))
        return act + " " + sep.join(cond)

    @_connection_handler
    def escape(self, text, args=None):
        """
        Format a string so it can be passed to psycopg2
        http://initd.org/psycopg/docs/cursor.html#cursor.mogrify
        """
        cur = self.cursor
        req = cur.mogrify(text, args)
        return req

    def _add_filter_cond(self, conditions, cond):
        if any(v.lower().startswith("where") for v in conditions):
            # If there is already a 'WHERE' condition, we add ours.
            return [(("%s AND %s" % (v, cond)) if
                    v.lower().startswith("where") else v) for v in conditions]
        else:
            return conditions + ("WHERE %s" % cond,)

    def _elem_type(type_):
        def dec(fun):
            def new_fun(self, columns, *conditions):
                conditions = self._add_filter_cond(conditions,
                        self.escape("type=%s", [type_]))
                return fun(self, columns, *conditions)
            return new_fun
        return dec

    def get_elem(self, columns="*", *conditions):
        """
        Returns the first post that matches the conditions (which must be given
        in SQL)
        See the filter function.
        """
        return self.filter("mdm_elem", True, columns, *conditions)

    def get_elems(self, columns="*", *conditions):
        """
        Like get_post but returns all the results in a list
        """
        return self.filter("mdm_elem", False, columns, *conditions)

    get_post = _elem_type(POST)(get_elem)
    get_posts = _elem_type(POST)(get_elems)
    get_comment = _elem_type(COMMENT)(get_elem)
    get_comments = _elem_type(COMMENT)(get_elems)

    def add_elem(self, **values):
        """
        Required values : 
            author (id),
            title,
            content,
            creation (datetime),
            update (same),

            karma (default to 0),
            draft (bool),
        """
        self.insert("mdm_elem", **values)

    def add_post(self, **values):
        values.update(type=POST)
        self.add_elem(**values)

    def add_comment(self, **values):
        values.update(type=COMMENT)
        self.add_elem(**values)

    def edit_elem(self, id, **values):
        self.edit("mdm_elem", {"id": id}, **values)

    def add_user(self, **values):
        """
        Required values :
            name
            password (sha256 hash)

            level (default to 0)
            email
            karma (default to 0)
        """
        self.insert("mdm_user", **values)

    def get_user(self, columns="*", *conditions):
        """
        """
        return self.filter("mdm_user", True, columns, *conditions)

    def edit_user(self, id, **values):
        self.edit("mdm_user", {"id": id}, **values)

    def del_user(self, id):
        self.edit("mdm_elem", {"author": id}, author=DELETED_USER)
        self.delete("mdm_user", self.escape("WHERE id=%s", [id]))

    def get_topic(self, name, approved=None, col="name"):
        """
        if approved is not None, the topic is only returned if approved matches.
        """
        d = {"name": name}
        if approved is not None:
            d.update(approved=approved)
        req = self.fmt("WHERE", **d)
        t = self.filter("mdm_topic", True, "*", req)
        return t[col] if t else None

    def get_topics(self, approved=None, post=None):
        """
        returns a list of topics.
        if approved is not None, returns only approved/not approved topics
        if post is not None, returns the topics of this post
        """
        get_col = lambda l, c: [e[c] for e in l]
        req = ""
        if approved is not None:
            req = self.escape("WHERE approved=%s", [approved])
        if post is None:
            return get_col(self.filter("mdm_topic", False, "(name)", req),
                    "name")
        else:
            cond = self.escape("WHERE elem_id=%s", [post])
            topics = get_col(self.filter("mdm_elem_topic", False, "(topic_id)",
                cond), "topic_id")
            if approved is not None:
                approved_topics = []
                for topic in topics:
                    t = self.get_topic(topic, approved)
                    if t:
                        approved_topics.append(t)
                return approved_topics
            else:
                return topics

    def register_topics(self, post, topics):
        """
        delete the topics associated to thist post and
        sets the topic names to the post_id,
        """
        self.delete("mdm_elem_topic", self.escape("WHERE elem_id=%s", [post]))
        for topic in topics:
            self.insert("mdm_elem_topic", elem_id=post, topic_id=topic)

    def set_karma(self, val, user_id=None, elem_id=None):
        cond = self.escape("WHERE id=%s", user_id if user_id else elem_id)
        id = user_id if user_id else elem_id
        table = "mdm_user" if user_id else "mdm_elem"
        karma = self.filter(table, True, "(karma)", cond)
        self.edit(table, {"id": id}, karma=karma+val)

    edit_post = edit_comment = edit_elem
