#!/usr/bin/env python
# -*- coding: utf-8 -*-

POST = 0
COMMENT = 1

DELETED = -2
BANNED = -1
ANONYMOUS = 0
USER = 1
MODERATOR = 2
ADMIN = 3
GOD = 42

SALT = "OOl6F%M2*=1KJJ;!-o<M3"
