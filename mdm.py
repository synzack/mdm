#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from database import Database
from structure import User, Elem, hash_pw
from constants import POST, COMMENT

from flask import Flask, request, session, g, redirect, url_for, abort, \
        render_template, flash, send_from_directory
from flaskext.babel import Babel, gettext, ngettext, lazy_gettext, \
        format_datetime

from markdown2 import Markdown
from functools import wraps
from datetime import datetime

markdowner = Markdown(extras=["code-color", "footnotes"])
markdown = lambda t: markdowner.convert(t)

__author__ = "^"
__version__ = -0.1

app = Flask(__name__)
app.config.from_pyfile("cfg.py")
app.config.from_envvar("FLASK_SETTINGS", True)
babel = Babel(app)

_ = gettext
__ = ngettext
___ = lazy_gettext

def login_required(f):
    """
    Decorator for a function that requires login
    It will redirect to the login page if the user is not logged in.
    """
    @wraps(f)
    def fun(*args, **kwargs):
        if not "user" in session:
            return redirect(url_for("login", next=request.url))
        return f(*args, **kwargs)
    return fun

@app.before_request
def before_request():
    """
    Before each request, we open a connection to the database
    It will be available through the variable g
    """
    g.db = Database()
    g.db.connect()

@app.after_request
def after_request(response):
    g.db.disconnect()
    return response

@app.template_filter('dateformat')
def jinja_format_date(d):
    """
    This is a filter for jinja2
    Render a date
    """
    return format_datetime(d, "short")

@app.template_filter('markdown')
def jinja_markdown(text):
    """
    returns html code
    """
    return markdown(text)

def get_form(form):
    """ Returns a simple function for getting things from a form """
    def get(val, default=None):
        return form.get(val, default)
    return get

def error(text):
    if hasattr(g, "error"):
        g.error.append(text)
    else:
        g.error = [text]

def get_posts(ppp=20, page=0, last=True):
    """
    Get ppp (posts per page) last posts on page 'page' if last is True,
    else get ppp first posts
    """
    esc = g.db.escape
    req = [esc("WHERE draft=%s AND visible=%s", (False, True))]
    req.append("ORDER BY creation %s" % ("DESC" if last else "ASC"))
    if page > -1 and ppp > -1:
        req.append(esc("LIMIT %s OFFSET %s", (ppp, ppp*page)))
    posts = g.db.get_posts("*", *req)
    f_posts = [Elem(set_values=True, from_db=False, **d) for d in posts]
    return f_posts

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
        'mdm.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/")
def index():
    posts = get_posts(5)
    return render_template("index.html", posts=posts)

@app.route("/m/")
@app.route("/m/page/<int:page>")
def list_posts(page=0):
    posts = get_posts(page=page)
    return render_template("listposts.html", posts=posts, page=page)

@app.route("/m/<int:post_id>")
def show_post(post_id):
    #post = g.db.get_post("*", g.db.fmt("WHERE", id=post_id))
    post = Elem(from_db=True, set_values=True, id=post_id)
    if not post.id:
        abort(404)
    if post.draft:
        # Only the author can see a draft
        if session.get("user", User()).id != post.author:
            abort(404)
    tree = post.make_tree()
    return render_template("viewpost.html", post=post, tree=tree)

def vote(elem_id, up=True):
    upvotes = session.setdefault("upvotes", [])
    if up and elem_id in upvotes:
        session["upvotes"].remove(elem_id)
        g.db.set_karma(-1, elem=elem_id)
    elif up:
        session["upvotes"].append(elem_id)
        g.db.set_karma(1, elem=elem_id)
    return elem_id in session["upvotes"]

@app.route("/vote/up/<int:elem_id>")
def upvote(elem_id):
    if vote(elem_id):
        return _("Upvoted")
    else:
        return _("Your previous vote has been removed")

@app.route("/new/post/", methods=["POST", "GET"])
@app.route("/new/", methods=["POST", "GET"])
def add_post():
    if request.method == "POST":
        f = request.form
        get = get_form(f)
        if get("content") and get("title") and (get("author") or "user" in
                session):
            if "user" in session:
                user = session["user"]
                draft = not ("published" in f)
            else:
                user = User(get("author"), new=True)
                draft = False
            elem = Elem(set_values=True, from_db=False, author=user.id, type=POST,
                    content=get("content"), title=get("title"), draft=draft)
            elem.add()
            post_id = elem.id
            if get("topics"):
                topics = f.getlist("topics")
                if not all(g.db.get_topic(name=t, approved=True) for t in
                        topics):
                    # If not all topics in the list exist or are approved
                    error(_("Some topics do not exist"))
                else:
                    g.db.register_topics(post_id, topics)
            return redirect(url_for("show_post", post_id=post_id))
        else:
            error(_("Title, author and content are required."))

    return render_template("addpost.html", topics=g.db.get_topics(approved=True))

@app.route("/new/comment/<int:elem_id>", methods=["POST", "GET"])
def add_comment(elem_id):
    if not elem_id or not g.db.get_elem("*", g.db.fmt("WHERE", id=elem_id)):
        abort(400)
    if request.method == "POST":
        f = request.form
        get = get_form(f)
        if get("content") and (get("author") or "user" in session):
            user = session["user"] if "user" in session else User(get("author"),
                    new=True)
            elem = Elem(set_values=True, from_db=False, author=user.id,
                    content=get("content"), type=COMMENT, response_to=elem_id)
            elem.add()
            comment_id = elem.id
            pid = get("post_id")
            if not pid:
                pid = elem.get_root().id
            return redirect(url_for("show_post", post_id=pid,
                comment_id=comment_id))
        else:
            error(_("Author and content are required"))
    base_elem = Elem(set_values=True, from_db=True, id=elem_id)
    return render_template("editcomment.html", base_elem=base_elem)


@app.route("/edit/post/", methods=["POST", "GET"])
@app.route("/edit/", methods=["POST", "GET"])
@app.route("/edit/post/<int:post_id>", methods=["POST", "GET"])
@app.route("/edit/<int:post_id>", methods=["POST", "GET"])
@login_required
def edit_post(post_id=None):
    user = session["user"]
    if not post_id:
        user_posts = g.db.get_posts("*", g.db.fmt("WHERE", author=user.id))
        if not user_posts:
            return redirect(url_for("add_post"))
        else:
            # We get the last post written by the user
            return redirect(url_for("edit_post", post_id=user_posts[-1]["id"]))
    post = Elem(set_values=True, from_db=True, id=post_id)
    #g.db.get_post("*", g.db.fmt("WHERE", id=post_id))
    if not post or post.author != user.id:
        abort(404)
    if request.method == "POST":
        f = request.form
        get = get_form(f)
        if get("topics"):
            topics = f.getlist("topics")
            if not all(g.db.get_topic(name=t, approved=True) for t in topics):
                # If not all topics in the list exist or are approved
                error(_("Some topics do not exist"))
            else:
                g.db.register_topics(post_id, topics)
        update = {}
        for v in ("content", "title"):
            if get(v) and get(v) != getattr(post, v):
                update[v] = get(v)
        if "published" in f and post.draft:
            update["draft"] = False
        elif not "published" in f and not post.draft:
            update["draft"] = True

        if update and not hasattr(g, "error"):
            date = datetime.now().isoformat()
            update.update(update=date)
            g.db.edit_post(id=post_id, **update)
            flash(_("Post updated"))
            return redirect(url_for("show_post", post_id=post_id))

    return render_template("editpost.html", post=post,
            topics=g.db.get_topics(approved=True))

@app.route("/login/", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        user = User(request.form['username'], request.form['password'])
        if user.auth:
            log_user(user)
            # then, we redirect to home or to the page that required a login
            flash(_("Welcome, %(user)s.", user=user.name))
            if "next" in request.args:
                target = request.args["next"]
            else:
                target = url_for("index")
            return redirect(target)
        else:
            error(_("Invalid username/password"))
    return render_template("login.html")

@app.route("/logout/")
def logout():
    session.pop("user", None)
    flash(_("You were logged out"))
    if "next" in request.args:
        target = request.args["next"]
    else:
        target = url_for("index")
    return redirect(target)

@app.route("/new/user/", methods=["POST", "GET"])
def add_user():
    if request.method == "POST":
        name = request.form["username"]
        password = request.form["password"]
        email = request.form["email"]
        user = User(name, password)
        if user.exists:
            error(_("This username is already taken."))
        else:
            user = User(name, password, email, new=True)
            log_user(user)
            flash(_("You were succefully logged in."))
            if "next" in request.args:
                target = request.args["target"]
            else:
                target = url_for("index")
            return redirect(target)

    return render_template("adduser.html")

@app.route("/edit/user/", methods=["POST", "GET"])
@login_required
def edit_user():
    if request.method == "POST":
        f = request.form
        user = session["user"]
        if "delete" in request.form:
            if f["delete"] == "confirm":
                # Phase 1: confirmations
                return render_template("edituser.html", confirm_delete=True)
            elif f["delete"] == "yes":
                # Phase 2 : delete all datas
                if "password" in f and User(user.name, f["password"]).auth:
                    g.db.del_user(id=user.id)
                    session.pop("user", None)
                    flash(_("Your account has been deleted :("))
                    return redirect(url_for("index"))
                else:
                    error(_("Wrong password."))
                    return render_template("edituser.html", confirm_delete=True)
        update = {}
        get = (lambda v,d=None,req=False: f.get(v, d) if f.get(v, d) or not
                req else d)
        if get("username", user.name, True) != user.name:
            if User(name=get("username")).exists:
                error(_("User exists"))
            else:
                update["name"] = get("username")
        if get("password1"):
            if get("password1") != get("password2"):
                error(_("Passwords do not match"))
            elif not User(user.name, get("old_password")).auth:
                error(_("Old password is wrong"))
            else:
                update["password"] = hash_pw(f["password1"])
        if get("email", user.email) != user.email:
            update["email"] = get("email")
        if get("infos", user.infos) != user.infos:
            update["infos"] = get("infos")
        if update and not hasattr(g, "error"):
            # Update the db, update user in session, and redirect
            g.db.edit_user(user.id, **update)
            flash(_("User has been updated"))
            session.pop("user", None)
            pw = hash_pw(get("password1")) if get("password1") else user.password
            name = get("username") if get("username") else user.name
            log_user(User(name, pw, hash=True))
            redirect(url_for("edit_user"))
    return render_template("edituser.html")

@app.route("/u/<int:user_id>")
def show_user(user_id):
    user = g.db.get_user("*", g.db.fmt("WHERE", id=user_id))
    if user["level"] < 1:
        abort(404)
    esc = g.db.escape
    req = [esc("WHERE draft=%s AND visible=%s AND author=%s", (False, True,
        user_id))]
    req.append("ORDER BY creation")
    #req.append(esc("LIMIT %s OFFSET %s", (ppp, ppp*page)))
    posts = g.db.get_posts("*", *req)
    return render_template("viewuser.html", user=user, posts=posts)

def log_user(user):
    session["user"] = user

if __name__ == "__main__":
    app.run("0.0.0.0")

