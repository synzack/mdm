
function select_topic(topic_name) {
    var elem = document.getElementById("topics");
    var selected = "";
    var cur = "";
    for (var i = 0; i < elem.length; i++) {
        cur = elem.options[i] 
        if (cur.selected)
            if (selected)
                selected = selected + ", " + cur.value;
            else
                selected = cur.value;
    }
    document.getElementById("selected_topics").value = selected;
}
