BEGIN;
CREATE TABLE "mdm_topic" (
    "name" varchar(50) NOT NULL PRIMARY KEY,
    "approved" boolean NOT NULL DEFAULT False
)
;
CREATE TABLE "mdm_user" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(30) NOT NULL, 
    "password" char(64) NOT NULL,       -- sha256 hash
    "email" varchar(60),                -- Only for password recovery
    "karma" bigint NOT NULL DEFAULT 0,  -- maybe this will be used.
    "level" integer DEFAULT 0,          -- scum, moderator, admin, dictator
    "infos" text                        -- Personnal description
)
;
CREATE TABLE "mdm_elem" (
    "id" serial NOT NULL PRIMARY KEY,
    "author" integer NOT NULL REFERENCES "mdm_user" ("id") DEFERRABLE INITIALLY DEFERRED,
    "title" text,
    "creation" timestamp NOT NULL,
    "update" timestamp NOT NULL,
    "content" text NOT NULL,
    "karma" bigint NOT NULL DEFAULT 0,
    "type" integer NOT NULL,        -- post or comment
    "draft" boolean NOT NULL DEFAULT True,
    "visible" boolean NOT NULL DEFAULT True,
    "response_to" integer REFERENCES "mdm_elem" ("id") DEFERRABLE INITIALLY DEFERRED 
) 
;
CREATE TABLE "mdm_elem_topic" (
    "id" serial NOT NULL PRIMARY KEY,
    "elem_id" integer NOT NULL REFERENCES "mdm_elem" ("id") DEFERRABLE INITIALLY DEFERRED,
    "topic_id" varchar(50) NOT NULL REFERENCES "mdm_topic" ("name") DEFERRABLE INITIALLY DEFERRED,
    UNIQUE ("elem_id", "topic_id")
)
;
/*

ALTER TABLE "engine_post_topic" ADD CONSTRAINT "post_id_refs_elem_ptr_id_3fce3695" FOREIGN KEY ("post_id") REFERENCES "engine_post" ("elem_ptr_id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "engine_post_response_to" ADD CONSTRAINT "post_id_refs_elem_ptr_id_152a3ddb" FOREIGN KEY ("post_id") REFERENCES "engine_post" ("elem_ptr_id") DEFERRABLE INITIALLY DEFERRED;
CREATE TABLE "engine_comment_response_to" (
    "id" serial NOT NULL PRIMARY KEY,
    "comment_id" integer NOT NULL,
    "elem_id" integer NOT NULL REFERENCES "engine_elem" ("id") DEFERRABLE INITIALLY DEFERRED,
    UNIQUE ("comment_id", "elem_id")
)
;
CREATE TABLE "engine_comment" (
    "elem_ptr_id" integer NOT NULL PRIMARY KEY REFERENCES "engine_elem" ("id") DEFERRABLE INITIALLY DEFERRED
)
;
ALTER TABLE "engine_comment_response_to" ADD CONSTRAINT "comment_id_refs_elem_ptr_id_24e26b6f" FOREIGN KEY ("comment_id") REFERENCES "engine_comment" ("elem_ptr_id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "engine_elem_author_id" ON "engine_elem" ("author_id");

*/

COMMIT;
