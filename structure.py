#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import urandom
from constants import SALT, ANONYMOUS, USER, COMMENT, POST
from hashlib import sha256
from database import Database
from datetime import datetime

class User:
    id = 0
    name = None
    password = None
    level = ANONYMOUS
    email = None
    karma = 0
    infos = None
    auth = False
    exists = False
    db = Database()
    # If the username already is in the db
    def __init__(self, name=None, password=None, email=None, new=False,
            level=USER, hash=False):
        """
        User(name, password) creates a structure of an user
            if name and password match the database and the user is not banned,
                self.auth is True and level, email, karma and infos are set.
            if name is in the database, self.exists is set to True
        User(name, password, email, True) creates a new user
            if new is True and the username isn't already in the database, it is 
                created in the db.
            if no password is given, the new user is anonymous (and we don't care
                if the name is already used in this case) and read only.
        hash is True if the password is already hashed.
        """
        db = self.db 
        db.connect()
        escape = db.escape
        if name and password and not new:
            self.update_values(name=name, password=(password if hash else 
                hash_pw(password)))
        if name and not self.auth:
            req = db.fmt("WHERE", name=name, level_gt=0)
            self.exists = (db.get_user("*", req) is not None)
            self.name = name
        if new and password and not self.exists:
            db.add_user(name=name, password=hash_pw(password), email=email,
                    level=level)
            self.update_values(name=name, password=hash_pw(password))
        elif new and name:
            # To be sure we retrieve the correct id, we set a unique key as 
            # infos
            key = make_key(42)
            db.add_user(name=name, password="", level=ANONYMOUS, infos=key)
            self.update_values(infos=key)

    def update_values(self, **args):
        """
        Sets the different values (self.name, self.level...) after
        extracting it from the db

        update_values(name="foo", password="6b4ead6e9aa048bbcab18ee08c135...")
        """
        db = self.db
        req = db.fmt("WHERE", **args)
        user = db.get_user("*", req)
        if user:
            for k in user:
                setattr(self, k, user[k])
            if self.level > 0:
                self.auth = True
            self.exists = True


class Elem:
    db = Database()
    id = None
    author = None
    user = None
    creation = None
    update = None
    type = None
    karma = None
    draft = None
    response_to = None
    title = None
    content = None
    topics = None
    visible = True
    def __init__(self, set_values=False, from_db=False, **args):
        """ Creates a new Post or Comment """
        if set_values:
            self.update_values(from_db, **args)

    def update_values(self, from_db=False, **args):
        db = self.db
        if from_db:
            req = db.fmt("WHERE", **args)
            elem = db.get_elem("*", req)
        else:
            elem = args
        if elem:
            for k in elem:
                setattr(self, k, elem[k])
            self.user = User()
            self.user.update_values(id=self.author)
            self.topics = db.get_topics(approved=True, post=self.id)

    def add(self):
        """ Add this element to the db, sets the id """
        date = datetime.now().isoformat()
        def set(v, d):
            if getattr(self, v) is None:
                setattr(self, v, d)
        set("creation", date)
        set("update", date)
        set("draft", False)
        set("visible", True)
        set("title", "")
        db = self.db
        val = ("author", "creation", "update", "content", "draft",
                "draft", "title", "type", "response_to")
        args = {}
        for v in val:
            args[v] = getattr(self, v)
        db.add_elem(**args)
        del args["response_to"]
        # XXX doesn't work with reponse_to = NULL, so we delete it
        # I suck at databases, so I don't understand this behavior
        self.update_values(True, **args)
    
    def get_root(self):
        """
        If this is a comment, returns the post id
        """
        if self.type == POST:
            return self.id
        elem = self
        while not elem.type == POST and elem.response_to:
            elem = Elem(True, True, id=elem.response_to)
        return elem

    def make_tree(self):
        """
        Returns a nested list with all comments to this elem """
        db = self.db
        children = [Elem(True, False, **elem) for elem in
                db.get_elems("*", db.fmt("WHERE", response_to=self.id,
                    type=COMMENT))]
        return [(c, c.make_tree()) for c in children]

def hash_pw(password):
    hsh = sha256(password)
    hsh.update(SALT)
    return hsh.hexdigest()

def make_key(length=50):
    return "".join(chr(o) for o in (((ord(c) % (127-32)) + 32) for c in
        urandom(length)))
